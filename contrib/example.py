#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from __future__ import print_function

import os
import sys
import time

from requests import Session

MAX_RETRIES = 5


def _upload(session: Session, filename: str) -> None:
    # upload
    try:
        with open(filename, "rb") as f:
            payload = {"target": "private"}
            for _ in range(MAX_RETRIES):
                rv = session.post(
                    f"{os.environ['LVFS_SERVER']}/lvfs/upload/",
                    data=payload,
                    files={"file": f},
                )
                if rv.status_code == 201:
                    return
                print(f"failed to upload to {rv.url}: {rv.text} [{rv.status_code}]")
                time.sleep(30)
    except IOError as e:
        print("Failed to load file", str(e))
        sys.exit(1)

    # no more retries
    sys.exit(1)


def _vendor_user_add(
    session: Session, vendor_id: str, username: str, display_name: str
) -> None:
    # upload
    payload = {"username": username, "display_name": display_name}
    for _ in range(MAX_RETRIES):
        rv = session.post(
            "/".join(
                [os.environ["LVFS_SERVER"], "lvfs", "vendor", vendor_id, "user", "add"]
            ),
            data=payload,
        )
        if rv.status_code == 200:
            return
        print(f"failed to create user using {rv.url}: {rv.text}")
        time.sleep(30)

    # no more retries
    sys.exit(1)


def _mdsync_import(session: Session, filename: str) -> None:
    # import
    try:
        with open(filename, "rb") as f:
            payload = f.read()
    except IOError as e:
        print("Failed to load file", str(e))
        sys.exit(1)
    for _ in range(MAX_RETRIES):
        rv = session.post(
            "/".join([os.environ["LVFS_SERVER"], "lvfs", "mdsync", "import"]),
            data=payload,
        )
        if rv.status_code == 200:
            print(f"imported {filename} successfully: {rv.text}")
            return
        print(f"failed to import mdsync using {rv.url}: {rv.text}")
        time.sleep(30)

    # no more retries
    sys.exit(1)


def _mdsync_export(session: Session, filename: str) -> None:
    # export
    for _ in range(MAX_RETRIES):
        rv = session.get(
            "/".join([os.environ["LVFS_SERVER"], "lvfs", "mdsync", "export"])
        )
        if rv.status_code == 200:
            try:
                with open(filename, "wb") as f:
                    f.write(rv.text.encode())
            except IOError as e:
                print("Failed to save file", str(e))
                sys.exit(1)
            print(f"exported {filename} successfully")
            return
        print(f"failed to export mdsync: {rv.status_code}")
        time.sleep(30)

    # no more retries
    sys.exit(1)


if __name__ == "__main__":
    # check required env variables are present
    for reqd_env in ["LVFS_PASSWORD", "LVFS_PASSWORD", "LVFS_SERVER"]:
        if reqd_env not in os.environ:
            print(f"Usage: {reqd_env} required")
            sys.exit(1)

    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} [upload|create-user|mdsync-import|mdsync-export]")
        sys.exit(1)

    # log in
    s = Session()
    data = {
        "username": os.environ["LVFS_USERNAME"],
        "password": os.environ["LVFS_PASSWORD"],
    }
    success = False
    for _ in range(MAX_RETRIES):
        r = s.post(f"{os.environ['LVFS_SERVER']}/lvfs/login", data=data)
        if r.status_code == 200:
            success = True
            break
        print(f"failed to login to {r.url}: {r.text}")
        time.sleep(30)

    # no more retries
    if not success:
        sys.exit(1)

    # different actions
    if sys.argv[1] == "upload":
        if len(sys.argv) != 3:
            print(f"Usage: {sys.argv[0]} upload filename")
            sys.exit(1)
        _upload(s, sys.argv[2])
    elif sys.argv[1] == "create-user":
        if len(sys.argv) != 5:
            print(f"Usage: {sys.argv[0]} upload vendor_id username display_name")
            sys.exit(1)
        _vendor_user_add(s, sys.argv[2], sys.argv[3], sys.argv[4])
    elif sys.argv[1] == "mdsync-import":
        if len(sys.argv) != 3:
            print(f"Usage: {sys.argv[0]} mdsync-import filename")
            sys.exit(1)
        _mdsync_import(s, sys.argv[2])
    elif sys.argv[1] == "mdsync-export":
        if len(sys.argv) != 3:
            print(f"Usage: {sys.argv[0]} mdsync-export filename")
            sys.exit(1)
        _mdsync_export(s, sys.argv[2])
    else:
        print("command not found!")
        sys.exit(1)

    # success
    sys.exit(0)

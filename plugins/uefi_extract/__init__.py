#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access

import tempfile
import glob
import os
import subprocess
import zlib
import uuid
import struct
import hashlib
from collections import namedtuple, defaultdict
from typing import Optional

from lvfs.pluginloader import (
    PluginBase,
    PluginSetting,
    PluginSettingBool,
    PluginSettingInt,
)
from lvfs.tests.models import Test
from lvfs.components.models import (
    Component,
    ComponentShard,
    ComponentShardAttribute,
    ComponentShardChecksum,
)
from lvfs.firmware.models import Firmware


def _string_to_bytes(line: str) -> bytes:
    return b"".join(
        [
            int(line[idx : idx + 2], 16).to_bytes(length=1, byteorder="big")
            for idx in range(0, len(line), 2)
        ]
    )


def _hash_like_acm(modulus: Optional[bytes], exponent: int = 0x10001) -> bytes:
    if not modulus:
        return struct.pack("<i", exponent)
    return modulus + struct.pack("<i", exponent)


class PfsFile:
    PFS_HEADER = "<8sII"
    PFS_FOOTER = "<II8s"
    PFS_SECTION = "<16sI4s8sQIIII16x"
    PFS_INFO = "<I16sHHHH4sH"

    def __init__(self) -> None:
        self.shards: list[ComponentShard] = []
        self._names: dict[str, str] = {}

    def _parse_model(self, blob: bytes) -> None:
        pass

    def _parse_info(self, blob: bytes) -> None:
        off = 0
        while off < len(blob):
            PfsInfoSection = namedtuple(
                "PfsInfoSection",
                [
                    "hdr_ver",
                    "guid",
                    "ver1",
                    "ver2",
                    "ver3",
                    "ver4",
                    "ver_type",
                    "charcnt",
                ],
            )
            try:
                pfs_info = PfsInfoSection._make(
                    struct.unpack_from(PfsFile.PFS_INFO, blob, off)
                )
            except struct.error as e:
                raise RuntimeError from e
            if pfs_info.hdr_ver != 1:
                raise RuntimeError(f"PFS info version {pfs_info.hdr_ver} unsupported")
            guid = str(uuid.UUID(bytes_le=pfs_info.guid))
            off += struct.calcsize(PfsFile.PFS_INFO)
            self._names[guid] = blob[off : off + pfs_info.charcnt * 2].decode(
                "utf-16-le"
            )
            off += pfs_info.charcnt * 2 + 2

    def parse(self, blob: bytes, md: Component) -> None:
        # sanity check
        PfsHeaderTuple = namedtuple(
            "PfsHeaderTuple", ["tag", "hdr_ver", "payload_size"]
        )
        try:
            pfs_hdr = PfsHeaderTuple._make(
                struct.unpack_from(PfsFile.PFS_HEADER, blob, 0x0)
            )
        except struct.error as e:
            raise RuntimeError from e
        if pfs_hdr.tag != b"PFS.HDR.":
            raise RuntimeError("Not a PFS header")
        if pfs_hdr.hdr_ver != 1:
            raise RuntimeError(f"PFS header version {pfs_hdr.hdr_ver} unsupported")

        # parse sections
        offset = struct.calcsize(PfsFile.PFS_HEADER)
        while offset < len(blob) - struct.calcsize(PfsFile.PFS_FOOTER):
            # parse the section
            PfsHeaderSection = namedtuple(
                "PfsHeaderSection",
                [
                    "guid",
                    "hdr_ver",
                    "ver_type",
                    "version",
                    "reserved",
                    "data_sz",
                    "data_sig_sz",
                    "metadata_sz",
                    "metadata_sig_sz",
                ],
            )
            try:
                pfs_sect = PfsHeaderSection._make(
                    struct.unpack_from(PfsFile.PFS_SECTION, blob, offset)
                )
            except struct.error as e:
                raise RuntimeError from e
            if pfs_sect.hdr_ver != 1:
                raise RuntimeError(f"PFS section version {pfs_hdr.hdr_ver} unsupported")
            offset += struct.calcsize(PfsFile.PFS_SECTION)

            # parse the data and ignore the rest
            shard = ComponentShard(component_id=md.component_id)
            shard_blob: bytes = blob[offset : offset + pfs_sect.data_sz]
            shard.write(shard_blob)
            shard.guid = str(uuid.UUID(bytes_le=pfs_sect.guid))
            if blob and shard.guid == "fd041960-0dc8-4b9f-8225-bba9e37c71e0":
                self._parse_info(shard_blob)
            elif blob and shard.guid == "233ae3fb-da68-4fd4-92cb-a6229a611d6f":
                self._parse_model(shard_blob)
            else:
                self.shards.append(shard)

            # advance to the next section
            offset += pfs_sect.data_sz
            offset += pfs_sect.data_sig_sz
            offset += pfs_sect.metadata_sz
            offset += pfs_sect.metadata_sig_sz

        # the INFO structure is typically last, so fix up added shards
        for shard in self.shards:
            if shard.guid in self._names:
                shard.name = "com.dell." + self._names[shard.guid].replace(" ", "")
            else:
                shard.name = "com.dell." + shard.guid


class InfoTxtFile:
    def __init__(self, blob: Optional[bytes] = None) -> None:
        self._data: dict[str, str] = {}
        if blob:
            self.parse(blob)

    def parse(self, blob: bytes) -> None:
        for line in blob.decode().split("\n"):
            try:
                k, v = line.split(": ", maxsplit=1)
                self._data[k] = v
            except ValueError:
                pass

    def get(self, key: str) -> Optional[str]:
        return self._data.get(key)

    def get_int(self, key: str) -> int:
        value = self.get(key)
        if not value:
            return 0
        for split in [",", " "]:
            value = value.split(split)[0]
        if value.endswith("h"):
            return int(value[:-1], 16)
        return int(value)


class PfatFile:
    PFAT_HEADER = "<II8sB"
    PFAT_BLOCK_HEADER = "<I16sIIIIIII"
    PFAT_BLOCK_SIGN = "<II64II64I"

    def __init__(self) -> None:
        self.shards: list[ComponentShard] = []

    def parse(self, blob: bytes, md: Component) -> None:
        # sanity check
        PfatHeader = namedtuple("PfatHeader", ["size", "csum", "tag", "ctrl"])
        try:
            pfat_hdr = PfatHeader._make(
                struct.unpack_from(PfatFile.PFAT_HEADER, blob, 0x0)
            )
        except struct.error as e:
            raise RuntimeError from e
        if pfat_hdr.tag != b"_AMIPFAT":
            raise RuntimeError("Not a PFAT header")

        # parse the header data which seems to be of the form:
        # "1 /B 4 ;BIOS_FV_BB.bin" where the blockcount is 4 in this example
        section_data = (
            blob[struct.calcsize(PfatFile.PFAT_HEADER) : pfat_hdr.size]
            .decode("utf-8")
            .splitlines()
        )
        PfatSection = namedtuple(
            "PfatSection", ["flash", "param", "blockcnt", "filename"]
        )
        sections: list[PfatSection] = []
        for entry in section_data[1:]:
            entry_data = entry.split(" ")
            sections.append(
                PfatSection(
                    flash=int(entry_data[0]),
                    param=entry_data[1],
                    blockcnt=int(entry_data[2]),
                    filename=entry_data[3][1:],
                )
            )

        # parse sections
        offset = pfat_hdr.size
        for section in sections:
            data = b""
            for _ in range(section.blockcnt):
                PfatBlockHeader = namedtuple(
                    "PfatBlockHeader",
                    [
                        "revision",
                        "platform",
                        "unknown0",
                        "unknown1",
                        "flagsz",
                        "datasz",
                        "unknown2",
                        "unknown3",
                        "unknown4",
                    ],
                )
                block_hdr = PfatBlockHeader._make(
                    struct.unpack_from(PfatFile.PFAT_BLOCK_HEADER, blob, offset)
                )
                block_data_start = (
                    offset
                    + struct.calcsize(PfatFile.PFAT_BLOCK_HEADER)
                    + block_hdr.flagsz
                )
                data += blob[block_data_start : block_data_start + block_hdr.datasz]
                offset += (
                    struct.calcsize(PfatFile.PFAT_BLOCK_HEADER)
                    + block_hdr.flagsz
                    + block_hdr.datasz
                    + struct.calcsize(PfatFile.PFAT_BLOCK_SIGN)
                )

            # add shard blob
            shard = ComponentShard(component_id=md.component_id)
            shard.write(data)
            shard.name = "com.ami." + section.filename
            shard.guid = str(uuid.uuid3(uuid.NAMESPACE_DNS, section.filename))
            self.shards.append(shard)


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "uefi-extract")
        self.name = "UEFI Extract"
        self.summary = "Add firmware shards for UEFI capsules"
        self.settings.append(
            PluginSettingBool(key="uefi_extract_enabled", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSetting(
                key="uefi_extract_binary",
                name="UEFIExtract executable",
                default="UEFIExtract",
            )
        )
        self.settings.append(
            PluginSettingInt(
                key="uefi_extract_size_min",
                name="Minimum size of shards",
                default=0x80000,
            )
        )  # 512kb
        self.settings.append(
            PluginSettingInt(
                key="uefi_extract_size_max",
                name="Maximum size of shards",
                default=0x4000000,
            )
        )  # 64Mb

    def _convert_report_to_shards(
        self, report: str, md: Component
    ) -> list[ComponentShard]:
        shards: list[ComponentShard] = []

        # ignore some lines
        lines: list[str] = []
        for line in report.split("\n"):
            if line.find("not a single Volume Top File is found") != -1:
                continue
            if not line:
                continue
            lines.append(line)
        if not lines:
            return shards

        # raw text contents
        shard = ComponentShard(
            plugin_id=self.id,
            component_id=md.component_id,
            name="com.github.LongSoft.UEFITool",
            guid=str(uuid.uuid5(uuid.NAMESPACE_DNS, "stdout")),
        )
        blob: bytes = report.encode()
        shard.write(blob)
        shards.append(shard)

        # parse stdout until we get this saved in the DUMP folder
        data: dict[str, bytes] = defaultdict(bytes)
        data_currkey: Optional[str] = None
        for line in lines:
            tokens = line.split(":")
            if len(tokens) == 1:
                if not data_currkey:
                    continue
                data[data_currkey] += _string_to_bytes(line)
                continue
            if tokens[0].strip() in [
                "Boot Policy Public Key",
                "Key Manifest Public Key",
                "ACM RSA Public Key",
            ]:
                data_currkey = tokens[0].strip()
                continue
            data_currkey = None

        # Boot Policy
        if "Boot Policy Public Key" in data:
            shard = ComponentShard(
                plugin_id=self.id,
                component_id=md.component_id,
                name="com.intel.BootGuard.BootPolicy",
                guid=str(uuid.uuid5(uuid.NAMESPACE_DNS, "IntelBootGuardBootPolicy")),
            )
            shard.write(data["Boot Policy Public Key"], checksums=[])
            shard.checksums.append(
                ComponentShardChecksum(
                    value=hashlib.sha256(_hash_like_acm(blob)).hexdigest(),
                    kind="SHA256",
                )
            )
            shard.checksums.append(
                ComponentShardChecksum(
                    value=hashlib.sha384(_hash_like_acm(blob)).hexdigest(),
                    kind="SHA384",
                )
            )
            shards.append(shard)

        # Key Manifest
        if "Key Manifest Public Key" in data:
            shard = ComponentShard(
                plugin_id=self.id,
                component_id=md.component_id,
                name="com.intel.BootGuard.KeyManifest",
                guid=str(uuid.uuid5(uuid.NAMESPACE_DNS, "IntelBootGuardKeyManifest")),
            )
            shard_blob: bytes = data["Key Manifest Public Key"]
            shard.checksums.append(
                ComponentShardChecksum(
                    value=hashlib.sha256(_hash_like_acm(shard_blob)).hexdigest(),
                    kind="SHA256",
                )
            )
            shard.checksums.append(
                ComponentShardChecksum(
                    value=hashlib.sha384(_hash_like_acm(shard_blob)).hexdigest(),
                    kind="SHA384",
                )
            )
            shards.append(shard)
            shard.write(shard_blob, checksums=[])

        # ACM RSA
        if "ACM RSA Public Key" in data:
            shard = ComponentShard(
                plugin_id=self.id,
                component_id=md.component_id,
                name="com.intel.Acm",
                guid=str(uuid.uuid5(uuid.NAMESPACE_DNS, "IntelBootGuardKeyAcmRsa")),
            )
            shards.append(shard)
            shard.write(data["ACM RSA Public Key"], checksums=["SHA256"])

        # success
        return shards

    def _convert_files_to_shards(
        self, files: list[str], md: Component
    ) -> list[ComponentShard]:
        # parse each EFI binary as a shard
        shards: list[ComponentShard] = []
        shard_by_checksum: dict[str, ComponentShard] = {}
        for fn in files:
            dirname = os.path.dirname(fn)
            try:
                with open(os.path.join(dirname, "body.bin"), "rb") as f:
                    payload = f.read()
            except FileNotFoundError:
                continue
            if len(payload) < 0x100:
                # print(f'ignoring payload of {len(payload)} bytes')
                continue

            # read in child data
            with open(fn, "rb") as f:
                data = InfoTxtFile(f.read())
            if data.get("Subtype") in ["PE32 image", "TE image"]:
                with open(os.path.join(dirname, "..", "info.txt"), "rb") as f:
                    data = InfoTxtFile(f.read())
            name = data.get("Text")
            if not name:
                if data.get("CPU signature") and data.get("CPU flags"):
                    name = f"{data.get_int('CPU signature'):08X}.{data.get_int('CPU flags'):08X}"
            if name:
                for src in [" ", "(", ")"]:
                    name = name.replace(src, "_")
            kind = data.get("Type")
            if not kind:
                continue
            subkind = data.get("Subtype")
            guid = data.get("File GUID")
            if guid:
                guid = guid.lower()
            if subkind:
                kind += "::" + subkind

            # generate something plausible
            if kind == "Microcode::Intel":
                guid = "3f0229ad-0a00-5269-90cf-0a45d8781b72"
            if not guid:
                # print('No GUID for', kind, fn)
                continue

            # ignore some kinds
            appstream_kinds = {
                "00h::Unknown 0": None,
                "01h::Compressed": None,
                "01h::Raw": None,
                "02h::Freeform": None,
                "02h::GUID defined": "org.uefi.Raw",
                "03h::SEC core": "org.uefi.Security",
                "04h::PEI core": "org.uefi.Pei",
                "05h::DXE core": "org.uefi.Dxe",
                "06h::PEI module": "org.uefi.Peim",
                "07h::DXE driver": "org.uefi.Driver",
                "09h::Application": "org.uefi.Application",
                "0Ah::SMM module": "org.uefi",
                "0Bh::Volume image": None,
                "0Ch::Combined SMM/DXE": "org.uefi.SmmDxe",
                "0Dh::SMM core": "org.uefi",
                "12h::TE image": None,
                "13h::DXE dependency": None,
                "14h::Version": None,
                "15h::UI": None,
                "17h::Volume image": None,
                "18h::Freeform subtype GUID": None,
                "19h::Raw": None,
                "1Bh::PEI dependency": None,
                "1Ch::MM dependency": None,
                "BPDT store": None,
                "CPD entry": None,
                "CPD partition::Code": None,
                "CPD partition::Key": None,
                "CPD partition::Manifest": None,
                "CPD partition::Metadata": None,
                "CPD store": None,  # ??
                "ECh": None,
                "EDh::GUID": None,
                "EEh::Name": None,
                "EFh::Data": None,
                "F0h::Pad": None,
                "Free space": None,
                "FTW store": None,
                "Image::Intel": None,
                "Image::UEFI": None,
                "Microcode::Intel": "com.intel.Microcode",
                "NVAR entry::Full": None,
                "Padding::Empty (0xFF)": None,
                "Padding::Non-empty": "org.uefi.Padding",
                "Region::BIOS": None,
                "Region::Descriptor": None,
                "Region::DevExp1": None,
                "Volume::FFSv2": None,
                "Volume::NVRAM": "org.uefi.NVRAM",
                "VSS2 store": None,
            }
            if kind not in appstream_kinds:
                if len(kind) > 3:
                    print("No appstream_kinds for", kind, fn)
                continue
            appstream_id = appstream_kinds[kind]
            if not appstream_id:
                # print('Ignoring appstream kind', kind)
                continue

            # something plausible
            if name:
                appstream_id += f".{name}"
            shard = ComponentShard(
                plugin_id=self.id,
                component_id=md.component_id,
                name=appstream_id,
                guid=guid,
            )
            shard.write(payload)

            # skip invalid shards
            if not shard.checksum:
                continue

            # do not add duplicates!
            if shard.checksum in shard_by_checksum:
                # print(f'skipping duplicate {guid}')
                continue

            # add attributes
            if kind == "Microcode::Intel":
                # e.g. 000806E9
                value = data.get_int("CPU signature")
                if value:
                    shard.attributes.append(
                        ComponentShardAttribute(
                            plugin_id=self.id, key="cpuid", value=f"{value:08X}"
                        )
                    )

                # e.g. C0
                value = data.get_int("CPU flags")
                if value:
                    shard.attributes.append(
                        ComponentShardAttribute(
                            plugin_id=self.id,
                            key="platform",
                            value=f"{value:08X}",
                        )
                    )

                # e.g. C6
                value = data.get_int("Revision")
                if value:
                    shard.attributes.append(
                        ComponentShardAttribute(
                            plugin_id=self.id,
                            key="version",
                            value=f"{value:08X}",
                        )
                    )

                # convert dd.mm.yyyy to yyyymmdd
                ddmmyyyy = data.get("Date")
                if ddmmyyyy:
                    split = ddmmyyyy.split(".")
                    date_iso = f"{split[2]}{split[1]}{split[0]}"
                    shard.attributes.append(
                        ComponentShardAttribute(
                            plugin_id=self.id, key="yyyymmdd", value=date_iso
                        )
                    )

                # combined size of header and body
                sz_bdy = data.get_int("Full size")
                sz_hdr = data.get_int("Header size")
                if sz_bdy and sz_hdr:
                    value = sz_bdy + sz_hdr
                    shard.attributes.append(
                        ComponentShardAttribute(
                            plugin_id=self.id, key="size", value=f"{value:08X}"
                        )
                    )

                # e.g. 98458A98
                value = data.get_int("Checksum")
                if value:
                    shard.attributes.append(
                        ComponentShardAttribute(
                            plugin_id=self.id,
                            key="checksum",
                            value=f"{value:08X}",
                        )
                    )

            shard_by_checksum[shard.checksum] = shard
            shards.append(shard)
        return shards

    def _get_shards_for_blob(
        self, test: Test, blob: Optional[bytes], md: Component
    ) -> list[ComponentShard]:
        shards: list[ComponentShard] = []

        # sanity check
        if not blob:
            return shards

        # write blob to temp file
        with tempfile.TemporaryDirectory(prefix="lvfs") as cwd:
            with tempfile.NamedTemporaryFile(
                mode="wb", prefix="lvfs_", suffix=".bin", dir=cwd, delete=False
            ) as src:
                src.write(blob)
                src.flush()

                # run UEFIExtract
                argv = [
                    self.get_setting("uefi_extract_binary", required=True),
                    src.name,
                ]
                try:
                    report = subprocess.run(
                        argv, cwd=cwd, check=True, capture_output=True
                    )
                except subprocess.CalledProcessError as e:
                    test.add_warn("Failed to decode file", str(e))
                    files = None
                    report = None
                else:
                    files = glob.glob(
                        src.name + ".dump" + "/**/info.txt", recursive=True
                    )
            if files:
                shards += self._convert_files_to_shards(files, md=md)
            if report:
                shards += self._convert_report_to_shards(report.stdout.decode(), md=md)
            return shards

    def _find_zlib_sections(self, blob: bytes) -> list[bytes]:
        offset = 0
        sections: list[bytes] = []
        while 1:
            # find Zlib header for default compression
            offset = blob.find(b"\x78\x9C", offset)
            if offset == -1:
                break

            # decompress the buffer, which also checks if it's really Zlib or just
            # two random bytes that happen to match for no good reason
            try:
                obj = zlib.decompressobj()
                blob_decompressed = obj.decompress(blob[offset:])
                offset = len(blob) - len(obj.unused_data)
            except zlib.error:
                offset += 2
            else:
                # only include blobs of a sane size
                if len(blob_decompressed) > self.get_setting_int(
                    "uefi_extract_size_min"
                ) and len(blob_decompressed) < self.get_setting_int(
                    "uefi_extract_size_max"
                ):
                    sections.append(blob_decompressed)
        return sections

    def _run_uefi_extract_on_md(self, test: Test, md: Component) -> None:

        # no data
        blob: Optional[bytes] = md.read()
        if not blob:
            return

        # is this a AMI BIOS with PFAT sections
        if blob[8:16] == b"_AMIPFAT":
            pfat = PfatFile()
            pfat.parse(blob, md=md)
            shards: list[ComponentShard] = []
            for shard in pfat.shards:
                shards.append(shard)
                if shard.name == "com.ami.BIOS_FV_BB.bin":
                    continue
                shards.extend(self._get_shards_for_blob(test, blob, md=md))
            test.add_pass("Found PFAT blob")

        # try with the plain blob (possibly with a capsule header) and
        # then look for a Zlib section (with an optional PFS-prefixed) blob
        else:
            shards = self._get_shards_for_blob(test, blob, md=md)
            if len(shards) <= 4:
                for blob2 in self._find_zlib_sections(blob):
                    try:
                        pfs = PfsFile()
                        pfs.parse(blob2, md=md)
                        for shard in pfs.shards:
                            shards.append(shard)
                            shards.extend(self._get_shards_for_blob(test, blob2, md=md))
                        test.add_pass("Found PFS in Zlib compressed blob")
                    except RuntimeError:
                        shard = ComponentShard(
                            plugin_id=self.id,
                            component_id=md.component_id,
                            name="Zlib",
                            guid="68b8cc0e-4664-5c7a-9ce3-8ed9b4ffbffb",
                        )
                        shard.write(blob2)
                        shards.append(shard)
                        shards.extend(self._get_shards_for_blob(test, blob2, md=md))
                        test.add_pass("Found Zlib compressed blob")
            if not shards:
                test.add_pass(f"No firmware volumes found in {md.filename_contents}")
                return

        # add shard to component if not already added
        shard_dedupe: dict[str, bool] = {}
        for shard in md.shards:
            if not shard.checksum:
                continue
            if shard.plugin_id in [self.id, "chipsec"]:
                shard_dedupe[shard.checksum] = shard
        for shard in shards:
            if not shard.checksum:
                continue
            if shard.checksum in shard_dedupe:
                continue
            shard_dedupe[shard.checksum] = True

            # actually add
            shard.plugin_id = self.id
            shard.component_id = md.component_id
            md.shards.append(shard)

    def require_test_for_md(self, md: Component) -> bool:
        if not md.protocol:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = fw.find_test_by_plugin_id("chipsec")  # old name
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        # extract the capsule data
        self._run_uefi_extract_on_md(test, md)


# run with PYTHONPATH=. ./env/bin/python3 plugins/uefi_extract/__init__.py
if __name__ == "__main__":
    import sys
    from lvfs.protocols.models import Protocol
    from lvfs import create_app

    with create_app().app_context():
        plugin = Plugin()
        plugin._setting_kvs["uefi_extract_size_min"] = "0"
        plugin._setting_kvs["uefi_extract_size_max"] = "1048576"
        plugin._setting_kvs["uefi_extract_binary"] = "/usr/bin/UEFIExtract"
        for _argv in sys.argv[1:]:
            print("Processing", _argv)
            _test = Test(plugin_id=plugin.id)
            _fw = Firmware()
            _md = Component()
            _md.component_id = 999999
            _md.filename_contents = "filename.bin"
            _md.protocol = Protocol(value="org.uefi.capsule")
            _fw.mds.append(_md)
            with open(_argv, "rb") as _f:
                _md.write(_f.read())
            for _md in _fw.mds:
                plugin.run_test_on_md(_test, _md)
            for attribute in _test.attributes:
                print(attribute)
            for _shard in _md.shards:
                print(_shard.guid, _shard.name)
                for _csum in _shard.checksums:
                    print(_csum.kind, _csum.value)
                for _attr in _shard.attributes:
                    print(_attr.key, _attr.value)

Security
########

There are many layers of security in the LVFS and fwupd design, including restricted account modes,
2 factor authentication, and server side AppStream namespaces.

The most powerful one is the so-called ``vendor-id`` that the vendors cannot assign themselves,
and is assigned by a member of the LVFS admin team when creating the vendor account on the LVFS.
The way this works is that all firmware from the vendor is tagged with a *requirement* like
``USB:0x056A`` which matches the USB consortium vendor assigned ID.

**Client side**, the ``vendor-id`` from the signed metadata is checked against the physical device
and the firmware is updated only if the ID matches.
This ensures that malicious or careless users on the LVFS can never ship firmware updates for other
vendors hardware.
All vendors on the LVFS are now locked down with this mechanism.

Some vendors have to use IDs that they do not own, a good example here is for a DFU device like
the 8BitDo controllers.
In runtime mode they use the USB-assigned 8BitDo VID, but in bootloader mode they use a generic VID
which is assigned to the chip supplier as they are using the reference bootloader.
This is obviously fine, and both vendor IDs are assigned to 8BitDo on the LVFS for this reason.

Another example is where Lenovo is responsible for updating Lenovo-specific NVMe firmware, but
where the NVMe vendor is not using the Lenovo PCI ID.

All devices exported by fwupd must have at least one vendor ID, mostly automatically added as the
vast majority derive from either ``FuUsbDevice`` or ``FuUdevDevice``.

The vendor IDs can be dispayed using ``fwupdmgr get-devices``.

UEFI UpdateCapsule
------------------

Capsule updates are a popular way to distribute firmware updates.
As the ESRT convays no vendor ownership information, we use the platform DMI data.
For instance Lenovo is only able to update Lenovo hardware with ``DMI:Lenovo``.

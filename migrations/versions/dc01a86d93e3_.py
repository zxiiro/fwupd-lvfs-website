"""empty message

Revision ID: dc01a86d93e3
Revises: c1c25da8e9f5
Create Date: 2023-11-06 16:37:26.932850

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "dc01a86d93e3"
down_revision = "c1c25da8e9f5"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
        batch_op.add_column(sa.Column("size", sa.Integer(), nullable=True))


def downgrade():
    with op.batch_alter_table("firmware_revisions", schema=None) as batch_op:
        batch_op.drop_column("size")

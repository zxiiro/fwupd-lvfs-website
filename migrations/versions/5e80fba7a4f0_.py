# Custom template
"""empty message

Revision ID: 5e80fba7a4f0
Revises: 90fb9c7b5f1a
Create Date: 2023-03-30 13:25:34.444972

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "5e80fba7a4f0"
down_revision = "90fb9c7b5f1a"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "client_acls",
        sa.Column("client_acl_id", sa.Integer(), nullable=False),
        sa.Column("addr", sa.Text(), nullable=False),
        sa.Column("message", sa.Text(), nullable=False),
        sa.Column("comment", sa.Text(), nullable=True),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("client_acl_id"),
    )
    with op.batch_alter_table("client_acls", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("ix_client_acls_addr"), ["addr"], unique=True)
    with op.batch_alter_table("clients", schema=None) as batch_op:
        batch_op.add_column(sa.Column("addr", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("clients", schema=None) as batch_op:
        batch_op.drop_column("addr")
    with op.batch_alter_table("client_acls", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_client_acls_addr"))
    op.drop_table("client_acls")

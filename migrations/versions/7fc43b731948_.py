"""empty message

Revision ID: 7fc43b731948
Revises: 3d7c893bbd0b
Create Date: 2023-08-17 13:04:08.242234

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7fc43b731948"
down_revision = "3d7c893bbd0b"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "vendor_relationships",
        sa.Column("vendor_relationships_id", sa.Integer(), nullable=False),
        sa.Column("timestamp", sa.DateTime(), nullable=False),
        sa.Column("vendor_id", sa.Integer(), nullable=False),
        sa.Column("vendor_id_odm", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id_odm"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("vendor_relationships_id"),
    )
    with op.batch_alter_table("vendor_relationships", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_vendor_relationships_vendor_id"), ["vendor_id"], unique=False
        )
        batch_op.create_index(
            batch_op.f("ix_vendor_relationships_vendor_id_odm"),
            ["vendor_id_odm"],
            unique=False,
        )
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.add_column(sa.Column("relationship_text", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.drop_column("relationship_text")
    with op.batch_alter_table("vendor_relationships", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_vendor_relationships_vendor_id_odm"))
        batch_op.drop_index(batch_op.f("ix_vendor_relationships_vendor_id"))
    op.drop_table("vendor_relationships")

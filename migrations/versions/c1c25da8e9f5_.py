"""empty message

Revision ID: c1c25da8e9f5
Revises: 8f383bb185ac
Create Date: 2023-09-22 09:29:14.479171

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "c1c25da8e9f5"
down_revision = "8f383bb185ac"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("protocol_device_flags", schema=None) as batch_op:
        batch_op.add_column(sa.Column("fwupd_version", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("protocol_device_flags", schema=None) as batch_op:
        batch_op.drop_column("fwupd_version")

# Custom template
"""empty message

Revision ID: 87d1787f2a63
Revises: a24ece15f5cf
Create Date: 2023-03-31 16:41:15.458332

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "87d1787f2a63"
down_revision = "a24ece15f5cf"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("geoips", schema=None) as batch_op:
        batch_op.alter_column(
            "addr_start",
            existing_type=sa.BIGINT(),
            type_=sa.Numeric(),
            existing_nullable=False,
        )
        batch_op.alter_column(
            "addr_end",
            existing_type=sa.BIGINT(),
            type_=sa.Numeric(),
            existing_nullable=False,
        )


def downgrade():
    with op.batch_alter_table("geoips", schema=None) as batch_op:
        batch_op.alter_column(
            "addr_end",
            existing_type=sa.Numeric(),
            type_=sa.BIGINT(),
            existing_nullable=False,
        )
        batch_op.alter_column(
            "addr_start",
            existing_type=sa.Numeric(),
            type_=sa.BIGINT(),
            existing_nullable=False,
        )

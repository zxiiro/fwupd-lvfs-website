"""empty message

Revision ID: 3bf16c5c54bb
Revises: e32bf2a0b74a
Create Date: 2023-01-09 11:43:57.269016

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "3bf16c5c54bb"
down_revision = "e32bf2a0b74a"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("firmware_limits", schema=None) as batch_op:
        batch_op.drop_index("ix_firmware_limits_firmware_id")
    op.drop_table("firmware_limits")


def downgrade():
    op.create_table(
        "firmware_limits",
        sa.Column(
            "firmware_limit_id", sa.INTEGER(), autoincrement=True, nullable=False
        ),
        sa.Column("firmware_id", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column("value", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column("ctime", postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
        sa.Column("user_agent_glob", sa.TEXT(), autoincrement=False, nullable=True),
        sa.Column("response", sa.TEXT(), autoincrement=False, nullable=True),
        sa.ForeignKeyConstraint(
            ["firmware_id"],
            ["firmware.firmware_id"],
            name="firmware_limits_firmware_id_fkey",
        ),
        sa.PrimaryKeyConstraint("firmware_limit_id", name="firmware_limits_pkey"),
    )
    with op.batch_alter_table("firmware_limits", schema=None) as batch_op:
        batch_op.create_index(
            "ix_firmware_limits_firmware_id", ["firmware_id"], unique=False
        )

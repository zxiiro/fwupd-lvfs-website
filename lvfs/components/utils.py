#!/usr/bin/python3
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import io
import socket
from zipfile import ZipFile, ZIP_DEFLATED

from flask import g
from uswid import (
    uSwidContainer,
    uSwidEntity,
    uSwidEntityRole,
    uSwidEvidence,
    uSwidFormatCycloneDX,
    uSwidFormatSpdx,
    uSwidFormatSwid,
    uSwidIdentity,
    uSwidLink,
)

from lvfs.components.models import Component

COMPONENT_SWID_REL_TO_DISPLAY = {
    "ancestor": "Ancestor",
    "compiler": "Compiler",
    "component": "Component",
    "feature": "Feature",
    "installationmedia": "Installation Media",
    "license": "License",
    "packageinstaller": "Package Installer",
    "parent": "Parent",
    "patches": "Patches",
    "requires": "Requires",
    "see-also": "See Also",
    "supersedes": "Supersedes",
    "supplemental": "Supplemental",
}


def _build_swid_identity_root(md: Component) -> uSwidIdentity:
    """create identity for the index with original vendor and LVFS"""

    identity = uSwidIdentity(
        tag_id=md.appstream_id,
        software_name=md.name,
        software_version=md.version_display,
        generator="LVFS",
    )
    identity.add_entity(
        uSwidEntity(
            name="LVFS",
            regid=g.host_name,
            roles=[uSwidEntityRole.TAG_CREATOR, uSwidEntityRole.DISTRIBUTOR],
        )
    )
    identity.add_entity(
        uSwidEntity(
            name=md.fw.vendor.display_name,
            regid=md.fw.vendor.regid,
            roles=[uSwidEntityRole.SOFTWARE_CREATOR],
        )
    )
    identity.add_entity(
        uSwidEntity(
            name=md.fw.vendor.display_name,
            regid=md.fw.vendor.regid,
            roles=[uSwidEntityRole.SOFTWARE_CREATOR],
        )
    )
    identity.add_evidence(uSwidEvidence(md.fw.timestamp, socket.getfqdn()))
    for swid in md.swids:
        identity.add_link(uSwidLink(rel="component", href=f"swid:{swid.tag_id}"))
    return identity


def _build_swid_archive(md: Component) -> bytes:
    buf = io.BytesIO()
    identity: uSwidIdentity = _build_swid_identity_root(md)
    with ZipFile(buf, mode="a", compression=ZIP_DEFLATED, allowZip64=False) as zf:
        zf.writestr("index.xml", uSwidFormatSwid().save(uSwidContainer([identity])))
        for swid in md.swids:
            zf.writestr(f"{swid.tag_id}.xml", swid.value)

        # mark the files as having been created on Windows
        for zfile in zf.filelist:
            zfile.create_system = 0

    return buf.getvalue()


def _build_swid_cyclonedx(md: Component) -> bytes:
    # create export
    container: uSwidContainer = uSwidContainer([_build_swid_identity_root(md)])
    for swid in md.swids:
        for identity in uSwidFormatSwid().load(swid.value.encode()):
            container.append(identity)
    return uSwidFormatCycloneDX().save(container)


def _build_swid_spdx(md: Component) -> bytes:
    # create export
    container: uSwidContainer = uSwidContainer([_build_swid_identity_root(md)])
    for swid in md.swids:
        for identity in uSwidFormatSwid().load(swid.value.encode()):
            container.append(identity)
    return uSwidFormatSpdx().save(container)
